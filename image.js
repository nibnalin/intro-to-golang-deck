import React from "react";

export default ({ custimg, style, giveCredit }) => {
  return (
    <div
      style={{
        width: "70%",
        paddingLeft: "15%"
      }}
    >
      <img
        src={custimg}
        alt="Gopher"
        style={{
          width: "100%",
          margin: "22% 0",
          borderRadius: "10px",
          transition: "0.8s",
          ...style
        }}
      ></img>
      {giveCredit && credit}
    </div>
  );
};

const credit = (
  <div style={{ fontSize: "12px", textAlign: "center" }}>
    Illustrations by{" "}
    <a
      target="_blank"
      rel="noopener noreferrer"
      href="https://github.com/MariaLetta/free-gophers-pack"
    >
      Maria Letta
    </a>{" "}
  </div>
);