package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Print("Go runs on ")
	os := runtime.GOOS
	if os == "darwin" {
		fmt.Println("OS X.")
	} else if os == "linux" {
		fmt.Println("Linux.")
	} else {
		fmt.Printf("%s.\n", os)
	}
}