package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Print("Go runs on ")
	if os := runtime.GOOS; os == "darwin" {
		fmt.Println("OS X.")
	} else if os == "linux" {
		fmt.Println("Linux.")
	} else {
		fmt.Printf("%s.\n", os)
	}
	// Can't use variable os here.
}