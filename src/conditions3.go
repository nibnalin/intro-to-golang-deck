package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Print("Go runs on ")
	os := runtime.GOOS
	switch {
	case os == "darwin":
		fmt.Println("OS X.")
	case os != "linux":
		fmt.Println("Not Linux.")
	default:
		fmt.Printf("%s.\n", os)
	}
}