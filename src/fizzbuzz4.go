package main

import "fmt"

var fizz, buzz, fizzbuzz, neither int

func fizzbuzzer(x int) {
	if x%3 == 0 && x%5 == 0 {
		fizzbuzz++
	}
}

func main() {
	for i := 0;i < 100;i++ {
		fizzbuzzer(i)
	}
	fmt.Println(fizz, buzz, fizzbuzz, neither)
}