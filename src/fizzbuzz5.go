package main

import "fmt"

var fizz, buzz, fizzbuzz, neither int

func fizzbuzzer(x int) {
	if x%3 == 0 && x%5 == 0 {
		fizzbuzz++
	} else if x%3 == 0 {
		fizz++
	} else if x%5 == 0 {
		buzz++
	} else {
		neither++
	}
}

func main() {
	for i := 0;i < 100;i++ {
		fizzbuzzer(i)
	}
	fmt.Println(fizz, buzz, fizzbuzz, neither)
}