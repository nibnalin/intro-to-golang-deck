package main

import "fmt"

func math(x int, y int) int {
	return x + y
}

func main() {
	fmt.Println(math(42, 13))
}