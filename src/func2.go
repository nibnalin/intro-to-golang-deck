package main

import "fmt"

func math(x, y int) (int, int) {
	return x + y, x - y
}

func main() {
	fmt.Println(math(42, 13))
}